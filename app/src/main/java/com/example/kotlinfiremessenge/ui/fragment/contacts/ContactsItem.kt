package com.example.kotlinfiremessenge.ui.fragment.contacts

import android.content.Context
import com.example.kotlinfiremessenge.R
import com.example.kotlinfiremessenge.glide.GlideApp
import com.example.kotlinfiremessenge.model.User
import com.example.kotlinfiremessenge.util.StorageUtil
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.item_person.*


class ContactsItem(val person: User,
                   val userId: String,
                   private val context: Context)
    : Item() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.textViewName.text = person.name
        viewHolder.textViewBio.text = person.bio
        if (person.profilePicturePath != null)
            GlideApp.with(context)
                .load(StorageUtil.pathToReference(person.profilePicturePath))
                .into(viewHolder.imageViewProfilePicture)
    }

    override fun getLayout() = R.layout.item_person
}