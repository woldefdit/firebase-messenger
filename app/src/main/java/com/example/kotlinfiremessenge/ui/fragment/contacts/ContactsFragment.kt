package com.example.kotlinfiremessenge.ui.fragment.contacts


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlinfiremessenge.R
import com.example.kotlinfiremessenge.util.AppConstants
import com.example.kotlinfiremessenge.ui.activities.chat.ChatActivity
import com.example.kotlinfiremessenge.util.FirestoreUtil
import com.google.firebase.firestore.ListenerRegistration
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.OnItemClickListener
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.fragment_contacts.*
import org.jetbrains.anko.support.v4.startActivity

class ContactsFragment : Fragment() {

    private lateinit var userListenerRegistration: ListenerRegistration

    private var shouldIninRecyclerView = true

    private lateinit var peopleSection: Section

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        userListenerRegistration = FirestoreUtil.addUserListener(this.activity!!, this::updateRecyclerView)

        return inflater.inflate(R.layout.fragment_contacts, container, false)


    }

    override fun onDestroy() {
        super.onDestroy()
        FirestoreUtil.removeListener(userListenerRegistration)
        shouldIninRecyclerView = true
    }

    private fun updateRecyclerView(items: List<Item>) {
        fun init() {
            recyclerViewPeople.apply {
                addItemDecoration(
                    DividerItemDecoration(
                        context,
                        DividerItemDecoration.VERTICAL
                    )
                )
                layoutManager = LinearLayoutManager(this@ContactsFragment.context)
                adapter = GroupAdapter<GroupieViewHolder>().apply {
                    peopleSection = Section(items)
                    add(peopleSection)
                    setOnItemClickListener(onItemClick)
                }
            }
            shouldIninRecyclerView = false
        }

        fun updateItems() = peopleSection.update(items)

        if (shouldIninRecyclerView)
            init()
        else
            updateItems()


    }

    private val onItemClick = OnItemClickListener { item, view ->
        if (item is ContactsItem){
            startActivity<ChatActivity>(
                AppConstants.USER_NAME to item.person.name,
                AppConstants.USER_IMAGE to item.person.profilePicturePath,
                AppConstants.USER_ID to item.userId


            )
        }
    }

}
