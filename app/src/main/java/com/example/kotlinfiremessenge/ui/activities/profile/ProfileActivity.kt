package com.example.kotlinfiremessenge.ui.activities.profile

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlinfiremessenge.R
import com.example.kotlinfiremessenge.glide.GlideApp
import com.example.kotlinfiremessenge.util.StorageUtil
import com.google.android.material.appbar.CollapsingToolbarLayout
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val collapsingToolbarLayout = findViewById<View>(R.id.collapsingToolbar) as CollapsingToolbarLayout

        val profileName: String = intent.getStringExtra("UserName")

        collapsingToolbarLayout.title = profileName

        bindUser()



        btn.setOnClickListener {
            val animation = AnimationUtils.loadAnimation(this, R.anim.rotate)
            collapsingToolbarLayout.startAnimation(animation)
        }
    }

    private fun bindUser() {

        val profileImage: String = intent.getStringExtra("ProfileImage")
        //profileUserName.text = profileName
        GlideApp.with(this)
            .load(StorageUtil.pathToReference(profileImage))
            .into(imageViewProfile)

        imageViewProfile.setOnClickListener {
            val intent = Intent(this, ImageActivity::class.java)
            intent.putExtra("ProfileImage", profileImage)
            startActivity(intent)
        }
    }


}
