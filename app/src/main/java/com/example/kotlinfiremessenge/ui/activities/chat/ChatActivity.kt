package com.example.kotlinfiremessenge.ui.activities.chat

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.kotlinfiremessenge.R
import com.example.kotlinfiremessenge.glide.GlideApp
import com.example.kotlinfiremessenge.model.ImageMessage
import com.example.kotlinfiremessenge.model.MessageType
import com.example.kotlinfiremessenge.model.TextMessage
import com.example.kotlinfiremessenge.ui.activities.profile.ImageActivity
import com.example.kotlinfiremessenge.ui.activities.profile.ProfileActivity
import com.example.kotlinfiremessenge.util.AppConstants
import com.example.kotlinfiremessenge.util.FirestoreUtil
import com.example.kotlinfiremessenge.util.StorageUtil
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ListenerRegistration
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Section
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.activity_chat.*
import java.io.ByteArrayOutputStream
import java.util.*

private const val RC_SELECT_IMAGE = 2

class ChatActivity : AppCompatActivity() {

    private lateinit var currentChannelId: String
    private var shouldIninRecyclerView = true
    private lateinit var messagesSection: Section
    private lateinit var messagesListenerRegistration: ListenerRegistration

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)


        toolbarImageButtonChatBackLatest.setOnClickListener {
            onBackPressed()
        }

        bindUser()

        val otherUserId = intent.getStringExtra(AppConstants.USER_ID)
        FirestoreUtil.getOrCreateChatChannel(otherUserId) { channelId ->
            currentChannelId = channelId
            messagesListenerRegistration =
                FirestoreUtil.addChatMessagesListener(channelId, this, this::updateRecyclerView)

            buttonSendMessageLog.setOnClickListener {
                val text = editTextMessage.text.toString()
                if (text.isEmpty()) {
                    return@setOnClickListener
                } else {
                    val messageToSend =
                        TextMessage(
                            editTextMessage.text.toString(), Calendar.getInstance().time,
                            FirebaseAuth.getInstance().currentUser!!.uid, MessageType.TEXT
                        )
                    editTextMessage.setText("")

                    FirestoreUtil.sendMessage(messageToSend, channelId)
                }
            }

            buttonSendImageLog.setOnClickListener {
                val intent = Intent().apply {
                    type = "image/*"
                    action = Intent.ACTION_PICK
                    putExtra(Intent.ACTION_SEND_MULTIPLE, arrayOf("image/jpeg", "image/png"))
                }
                startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    RC_SELECT_IMAGE
                )
            }
        }

    }

    private fun bindUser() {
        val userName = intent.getStringExtra(AppConstants.USER_NAME)
        val profileImage = intent.getStringExtra(AppConstants.USER_IMAGE)
        toolbarTextViewUserName.text = userName
        if (profileImage != null) {
            GlideApp.with(this)
                .load(StorageUtil.pathToReference(profileImage))
                .into(toolbarImageProfile)
        }
        toolbarTextViewUserName.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            intent.putExtra("UserName", userName)
            intent.putExtra("ProfileImage", profileImage)
            startActivity(intent)
        }

        toolbarImageProfile.setOnClickListener {
            val intent = Intent(this, ImageActivity::class.java)
            intent.putExtra("ProfileImage", profileImage)
            startActivity(intent)
        }

    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SELECT_IMAGE && resultCode == Activity.RESULT_OK && data != null && data.data != null) {

            val selectedImagePath = data.data

            val selectedImageBmp =
                MediaStore.Images.Media.getBitmap(contentResolver, selectedImagePath)

            val outputStream = ByteArrayOutputStream()

            selectedImageBmp.compress(Bitmap.CompressFormat.JPEG, 90, outputStream)
            val selectedImageBytes = outputStream.toByteArray()

            StorageUtil.uploadMessageImage(selectedImageBytes) { imagePath ->
                val messageToSend =
                    ImageMessage(
                        imagePath, Calendar.getInstance().time,
                        FirebaseAuth.getInstance().currentUser!!.uid
                    )

                FirestoreUtil.sendMessage(messageToSend, currentChannelId)
            }
        }
    }

    private fun updateRecyclerView(messages: List<Item>) {
        fun init() {
            recyclerViewMessages.apply {
                layoutManager = LinearLayoutManager(this@ChatActivity)
                adapter = GroupAdapter<GroupieViewHolder>().apply {
                    messagesSection = Section(messages)
                    this.add(messagesSection)
                }
            }
            shouldIninRecyclerView = false
        }

        fun updateItems() = messagesSection.update(messages)

        if (shouldIninRecyclerView)
            init()
        else
            updateItems()

        recyclerViewMessages.scrollToPosition(recyclerViewMessages.adapter!!.itemCount - 1)
    }

}
