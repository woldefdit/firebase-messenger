package com.example.kotlinfiremessenge.ui.activities.chat

import android.content.Context
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.kotlinfiremessenge.R
import com.example.kotlinfiremessenge.glide.GlideApp
import com.example.kotlinfiremessenge.model.ImageMessage
import com.example.kotlinfiremessenge.util.StorageUtil
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import kotlinx.android.synthetic.main.item_image_message.*

class ImageMessageItem(val message: ImageMessage,
                       val context: Context)
    : MessageItem(message){

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        super.bind(viewHolder, position)

        var requestOptions = RequestOptions()
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(12))

        GlideApp.with(context)
            .load(StorageUtil.pathToReference(message.imagePath))
            .centerCrop()
            .apply(requestOptions)
            .placeholder(R.drawable.ic_image_black_24dp)
            .into(viewHolder.imageViewMessageImage)

    }

    override fun getLayout() = R.layout.item_image_message

    override fun isSameAs(other: com.xwray.groupie.Item<*>?): Boolean {

        if (other !is ImageMessageItem)
            return false

        if (this.message != other.message)
            return false

        return true
    }

    override fun equals(other: Any?): Boolean {
        return isSameAs(other as? ImageMessageItem)
    }

    override fun hashCode(): Int {
        var result = message.hashCode()
        result = 31 * result + context.hashCode()
        return result
    }
}