package com.example.kotlinfiremessenge.ui.activities.splash

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlinfiremessenge.R
import com.example.kotlinfiremessenge.ui.activities.MainActivity
import com.example.kotlinfiremessenge.ui.activities.singin.SingInActivity
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.startActivity


class SplashActivity : AppCompatActivity() {

    private var mDelayedHandler: Handler ?= null
    private var SPLASH_DELAY: Long = 2000

    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing){
            if (FirebaseAuth.getInstance().currentUser == null)
                startActivity<SingInActivity>()
            else
                startActivity<MainActivity>()
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        mDelayedHandler = Handler()

        mDelayedHandler!!.postDelayed(mRunnable, SPLASH_DELAY)

    }


}