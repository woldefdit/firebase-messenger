package com.example.kotlinfiremessenge.ui.activities.profile

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.kotlinfiremessenge.R
import com.example.kotlinfiremessenge.glide.GlideApp
import com.example.kotlinfiremessenge.util.StorageUtil
import kotlinx.android.synthetic.main.activity_image.*

class ImageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        val profileImage: String = intent.getStringExtra("ProfileImage")

        GlideApp.with(this)
            .load(StorageUtil.pathToReference(profileImage))
            .into(imageView)
    }
}
