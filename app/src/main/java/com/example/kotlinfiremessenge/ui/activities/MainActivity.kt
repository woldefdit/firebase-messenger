package com.example.kotlinfiremessenge.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.kotlinfiremessenge.R
import com.example.kotlinfiremessenge.ui.fragment.account.MyAccountFragment
import com.example.kotlinfiremessenge.ui.fragment.contacts.ContactsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        replaceFragment(ContactsFragment())

        val navigationView: BottomNavigationView = findViewById(R.id.navigation)
        navigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigationPeople -> {
                    replaceFragment(ContactsFragment())
                    true
                }

                R.id.navigationMyAccount -> {
                    replaceFragment(MyAccountFragment())
                    true

                }
                else -> false
            }
        }
    }

    private fun replaceFragment(fragment: Fragment){
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentLayout, fragment)
            .commit()
    }

}