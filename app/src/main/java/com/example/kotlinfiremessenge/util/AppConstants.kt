package com.example.kotlinfiremessenge.util

object AppConstants {
    const val USER_NAME = "USER_NAME"
    const val USER_IMAGE = "USER_IMAGE"
    const val USER_ID = "USER_ID"
}