package com.example.kotlinfiremessenge.model

data class ChatChannel(val userId: MutableList<String?>) {
    constructor() : this(mutableListOf())
}